import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class ArrayD {
    public static void main(String[] args) {
        int value;
        value = inputIntFromConsole("Enter a number: ");
        int[] array = new int[value];
        initialize(array);
        List<Integer> sums = sum(array);
        display(array);
        System.out.println(sums);
    }

    public static void initialize(int[] arr) {
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(100);
        }
    }
    public static int inputIntFromConsole(String message) {
        int value;
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        value = sc.nextInt();
        return value;
    }
    public static List<Integer> sum(int[] array){
        List<Integer> sums = new LinkedList<>();
        int sum = 0;
        for(int i = 0; i < array.length; i++){
            if (i + 2 < array.length){
                sum = sum + array[i] + array[i + 1] + array[i + 2];
                sums.add(sum);
                sum = 0;
            }
        }
        return sums;
    }
    public static void display(int[] array){
        for (int element : array) {
            System.out.print(element + " ");
        }
    }
}